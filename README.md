# Meraki Orders Update API

URL: https://connect.boomi.com/ws/simple/getMerakiUpdate <br>
Authentication: Basic {{token}} <br>

Instructions on how to setup the Authorization headers can be found on the following [link](https://gcomm.my.salesforce.com/kA60K000000Hnpb).

## Request

The request should be a JSON of the form:

```javascript
{
	"serviceOrder" : "SO-12345678",
	"description" : "blablblabla",
	"MerakiSN" : "Q2TN-M63D-E2KU"
}
```

## Logic

The process will perform the following logic:

```mermaid
graph LR;
  id0["Receive Request"]-->id1["Lookup SO by Name"];
  id1-->|"fail"|id2["Return 'SO not Found' error message" ];
  id1-->|"success"|id3["Check the Status of the SO"];
  id3-->|"SO is closed"|id4["Return 'SO is Closed' error message"];
  id3-->|"SO is not closed"|id5[Get the related Cases and WOs];
  id5-->id6["1. Update the related Main Internal case"];
  id5-->id7["2. Set the Statuses to 'Equipment Configured'"];
  id5-->id8["3. Assign the Serial"];
  style id1 stroke-width:5px
  style id6 stroke-width:5px
  style id7 stroke-width:5px
  style id8 stroke-width:5px
```

1. Update the related Main Internal Case
*  Change the **Case Record Type** to PBX Provisioning
*  Create a new Case Comment containing `description`
2. Set the Statuses to 'Equipment Configured'
*  Change the **Status** of all WOs to 'Equipment Configured'
3. Assign the Serial
*  **Equipment Name** = 'Meraki Z3 Teleworker Gateway' 
*  **Serial Name** = `MerakiSN`

asdasd




Lookup is performed based on the `serviceOrder` while the Case Comment will be populated from `description`.

## Responses

The general form of the response is
```javascript
{
    "success" : "yes/no",
    "errorMessage" : "Error Message...."
    "successMessage" : "Success Message...."
}
```

### Service Order not Found
```javascript
{
    "success" : "no",
    "errorMessage" : "Service order `serviceOrder` not found."
}
```

### Service Order is Closed
```javascript
{
    "success" : "no",
    "errorMessage" : "Service order `serviceOrder` is closed."
}
```


### Success
```javascript
{
    "success" : "yes",
    "successMessage" : "Service Order `serviceOrder` found. Status changed to Equipment Configured on the following Work Orders: `workOrderList`."
}
```

## Sample Request

#### Request

```
POST /ws/simple/getMerakiUpdate HTTP/1.1
Host: connect.boomi.com
Content-Type: application/json
Authorization: {{auth token}}
User-Agent: PostmanRuntime/7.16.3
Accept: */*
Host: connect.boomi.com
Accept-Encoding: gzip, deflate
Content-Length: 91
Connection: keep-alive

{"serviceOrder" : "SO-0012687","description" : "blablblabla","MerakiSN" : "Q2TN-M63D-E2KU"}
```

#### Response

```javascript
{
  "success": "yes",
  "successMessage": "Service Order SO-0012687 found. Status changed to Equipment Configured on the following Work Orders: WO-00043273, WO-00043275, WO-00043276, WO-00043277, WO-00043278, WO-00043279, WO-00043280, WO-00043281, WO-00043282, WO-00043283, WO-00043284."
}
```






